rm -rf /tmp/terraform





export TF_VAR_image=foo



export TF_VAR_vsphere_user="$GOVC_USERNAME"
export TF_VAR_vsphere_password="$GOVC_PASSWORD"
export TF_VAR_vsphere_server="$(echo $GOVC_URL | sed 's@https://@@g')"
export TF_LOG=DEBUG
export TF_LOG_PATH=/tmp/terraform
env | grep TF_
terraform plan && terraform apply -auto-approve
