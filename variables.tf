variable "vsphere_user" {}
variable "vsphere_password" {}
variable "vsphere_server" {}
variable "cluster" {
    description = "cluster to run created vm on"
    default = "Cluster 1/Resources"
}

variable "cpus" {
    description = "number of cpus for each vm"
    default = 4
}

variable "datacenter" {
    description = "datacenter for created vm"
    default = "Datacenter"
}

variable "datastore" {
    description = "datastore for created vm"
    default = "FreeNAS"
}

variable "disk0" {
    description = "size of first disk in Gb"
    default = "32"
}

variable "domain" {
    description = "domain to use for created vm"
    default = "adp.darkwolf.io"
}

variable "folder" {
    description = "folder to create vm's in"
    default = "ADP/test2"
}

variable "gateway" {
    description = "address to use as gateway for created vm"
    default = "10.1.64.1"
}

variable "guest_id" {
    description = "vmware guest id for created vm"
    default = "rhel7_64Guest"
}

variable "memory" {
    description = "memory allocation for created vm"
    default = "8"
}

variable "name" {
    description = "root of virtual machine name"
    default = "docker"
}

variable "network" {
    description = "root of virtual machine name"
    default = "network/ADP"
}

variable "dns_servers" {
  type    = "list"
  default = ["10.1.0.1"]
}

variable "template" {
    description = "template to clone"
    default = "Templates/centos7_64Guest-template"
}

variable "timezone" {
    description = "timezone for vm's"
    default = "US/Central"
}

variable "vms" {
    description = "number of vm's to create"
    default = "3"
}

variable "ip_addresses" {
  default = {
    "0" = "10.1.64.114"
    "1" = "10.1.64.108"
    "2" = "10.1.64.115"
  }
}

