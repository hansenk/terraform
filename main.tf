
data "vsphere_datacenter" "dc" {
  name          = "${var.datacenter}"
}

data "vsphere_datastore" "datastore" {
  name          = "${var.datastore}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "pool" {
  name          = "${var.cluster}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name          = "${var.network}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = "${var.template}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_folder" "folder" {
  path          = "${var.folder}"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_virtual_machine" "vm" {
  name             = "${var.name}${count.index + 1}"
  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  num_cpus         = "${var.cpus}"
  memory           = "${var.memory}"
  guest_id         = "${var.guest_id}"
  folder           = "${vsphere_folder.folder.path}"
  scsi_type        = "${data.vsphere_virtual_machine.template.scsi_type}"
  count            = "${var.vms}"
  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
    label = "disk0"
    size  = "${var.disk0}"
    thin_provisioned  = false
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "${var.name}${count.index + 1}"
        domain    = "${var.domain}"
        time_zone = "${var.timezone}"
      }
      network_interface {
         ipv4_address = "${lookup(var.ip_addresses, count.index)}"
         ipv4_netmask = 24
         dns_server_list = "${var.dns_servers}"
      }

      ipv4_gateway = "${var.gateway}"
      dns_server_list = "${var.dns_servers}"
      dns_suffix_list = ["${var.domain}"]

    }
  }
}

